package com.example.hibernate_task.data;

import com.example.hibernate_task.models.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillRepository extends JpaRepository<Skill, Long> {

}
