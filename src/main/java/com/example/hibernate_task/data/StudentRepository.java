package com.example.hibernate_task.data;

import com.example.hibernate_task.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository <Student, Long> {
}
