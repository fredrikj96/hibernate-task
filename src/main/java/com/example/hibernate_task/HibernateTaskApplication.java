package com.example.hibernate_task;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info= @Info(title = "Teacher Skills And Students API", version = "1.0", description = "Information about teachers, their skills and students"))
public class HibernateTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibernateTaskApplication.class, args);
    }

}
