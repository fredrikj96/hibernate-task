package com.example.hibernate_task.controllers;

import com.example.hibernate_task.data.SkillRepository;
import com.example.hibernate_task.models.Skill;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/skills")
@Tag(name= "skills", description = "This controls operations for skills of the teachers")
public class SkillController {


    @Autowired
    private SkillRepository skillRepository;

    @Operation(summary = "This gets all skills")
    @GetMapping()
    public ResponseEntity<List<Skill>> getAllSkills(){
        List<Skill> skills = skillRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(skills,status);
    }

    @Operation(summary = "This gets the skills of a teacher with his/hers corresponding ID")
    @GetMapping("/{id}")
    public ResponseEntity<Skill> getSkills(@PathVariable Long id){
        Skill returnSkill = new Skill();
        HttpStatus status;


        if(skillRepository.existsById(id)){
            status = HttpStatus.OK;
            returnSkill = skillRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnSkill, status);
    }

    @Operation(summary = "This adds a new skill to the API")
    @PostMapping
    public ResponseEntity<Skill> addSkills(@RequestBody Skill skill){
        Skill returnSkill = skillRepository.save(skill);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnSkill, status);
    }

    @Operation(summary = "This updates an existing skill in the API")
    @PutMapping("/{id}")
    public ResponseEntity<Skill> updateSkills(@PathVariable Long id, @RequestBody Skill skill){
        Skill returnSkill = new Skill();
        HttpStatus status;


        if(!id.equals(skill.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnSkill,status);
        }
        returnSkill = skillRepository.save(skill);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnSkill, status);
    }


}
