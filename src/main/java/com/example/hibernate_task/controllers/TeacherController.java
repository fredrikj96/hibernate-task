package com.example.hibernate_task.controllers;

import com.example.hibernate_task.data.TeacherRepository;
import com.example.hibernate_task.models.Teacher;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/teachers")
@Tag(name= "teachers", description = "This controls operations for teachers")
public class TeacherController {

    @Autowired
    private TeacherRepository teacherRepository;

    @Operation(summary = "This gets all teachers")
    @GetMapping()
    public ResponseEntity<List<Teacher>> getAllTeachers(){
        List<Teacher> teachers = teacherRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(teachers,status);
    }

    @Operation(summary = "This gets a teacher with his/hers corresponding ID")
    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeachers(@PathVariable Long id){
        Teacher returnTeacher = new Teacher();
        HttpStatus status;

        if(teacherRepository.existsById(id)){
            status = HttpStatus.OK;
            returnTeacher = teacherRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnTeacher, status);
    }

    @Operation(summary = "This adds a new teacher to the API")
    @PostMapping
    public ResponseEntity<Teacher> addTeachers(@RequestBody Teacher teacher){
        Teacher returnTeacher = teacherRepository.save(teacher);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnTeacher, status);
    }

    @Operation(summary = "This updates an existing teacher in the API")
    @PutMapping("/{id}")
    public ResponseEntity<Teacher> updateTeachers(@PathVariable Long id, @RequestBody Teacher teacher){
        Teacher returnTeacher = new Teacher();
        HttpStatus status;

        if(!id.equals(teacher.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnTeacher,status);
        }
        returnTeacher = teacherRepository.save(teacher);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnTeacher, status);
    }

}
