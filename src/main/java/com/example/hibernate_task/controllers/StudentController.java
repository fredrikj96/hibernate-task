package com.example.hibernate_task.controllers;

import com.example.hibernate_task.data.StudentRepository;
import com.example.hibernate_task.models.Student;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/students")
@Tag(name= "students", description = "This controls operations for students")
public class StudentController {


    @Autowired
    private StudentRepository studentRepository;

    @Operation(summary = "This gets all students")

    @GetMapping()
    public ResponseEntity<List<Student>> getAllStudents(){
        List<Student> students = studentRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(students,status);
    }

    @Operation(summary = "This gets a student with his/hers corresponding ID")
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudents(@PathVariable Long id){
        Student returnStud = new Student();
        HttpStatus status;


        if(studentRepository.existsById(id)){
            status = HttpStatus.OK;
            returnStud = studentRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnStud, status);
    }

    @Operation(summary = "This adds a new student to the API")
    @PostMapping
    public ResponseEntity<Student> addStudents(@RequestBody Student student){
        Student returnStudent = studentRepository.save(student);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnStudent, status);
    }

    @Operation(summary = "This updates an existing student in the API")
    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudents(@PathVariable Long id, @RequestBody Student student){
        Student returnStudent = new Student();
        HttpStatus status;


        if(!id.equals(student.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnStudent,status);
        }
        returnStudent = studentRepository.save(student);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnStudent, status);
    }

}
