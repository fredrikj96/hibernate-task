package com.example.hibernate_task.models;


import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "skill")
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "skill")
    private String skill;

    @ManyToMany
    @JoinTable(
            name = "skillOf_teacher",
            joinColumns = @JoinColumn(name = "skill_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id")
    )
    public List<Teacher> teachers;

    @JsonGetter("teachers")
    public List<String> teachers() {
        return teachers.stream()
                .map(teacher -> {
                    return "/api/v1/teacher/" + teacher.getId();
                }).collect(Collectors.toList());
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }
}
