package com.example.hibernate_task.models;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "Teacher")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;


@OneToMany
@JoinColumn(name = "teacher_id")
List <Student> students;


@JsonGetter("students")
public List<String> students() {
    if(students != null) {
        return students.stream()
                .map(student -> {
                    return "/api/v1/students/" + student.getId();
                }).collect(Collectors.toList());
    }
    return null;
}

    @ManyToMany
    @JoinTable(
            name = "skillOf_teacher",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id")
    )
    public List<Skill> skills;

    @JsonGetter("skill")
    public List<String> skills() {
        if (skills != null) {
            return skills.stream()
                    .map(skill -> {
                        return "/api/v1/skill/" + skill.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
