package com.example.hibernate_task.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

@Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;


@ManyToOne
@JoinColumn(name = "teacher_id")
public Teacher teacher;


@JsonGetter("teacher")
public String teacher() {
    if(teacher != null){
        return "/api/v1/teachers/" + teacher.getId();
    }else{
        return null;
    }
}


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}